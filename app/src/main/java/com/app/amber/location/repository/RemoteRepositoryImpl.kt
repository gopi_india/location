package com.app.amber.location.repository

import android.content.Context
import com.app.amber.location.api.WeatherApi
import com.app.amber.location.model.request.WeatherRequest
import com.app.amber.location.model.response.WeatherResponse
import com.app.amber.location.util.AppResult
import com.app.amber.location.util.NetworkManager.isOnline
import com.app.amber.location.util.Utils.handleApiError
import com.app.amber.location.util.Utils.handleSuccess
import com.app.amber.location.util.noNetworkConnectivityError
import retrofit2.Response


class RemoteRepositoryImpl(
    private val api: WeatherApi,
    private val context: Context,
) : RemoteRepository {

    override suspend fun weatherApiCall(param: WeatherRequest): AppResult<WeatherResponse> {
        if (isOnline(context)) {
            return try {
                param.let {
                    var response: Response<WeatherResponse>? = null
                    response = api.weatherCall(it.lat!!, it.lon!!, it.appId!!)
                    if (response.isSuccessful) {
                        handleSuccess(response)
                    } else {
                        handleApiError(response)
                    }
                }
            } catch (e: Exception) {
                AppResult.Error(e)
            }
        } else {

            return context.noNetworkConnectivityError()
        }
    }


}
