package com.app.amber.location

import android.app.Application
import com.app.amber.location.di.apiModule
import com.app.amber.location.di.networkModule
import com.app.amber.location.di.repositoryModule
import com.app.amber.location.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class LocationApp : Application() {


    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@LocationApp)
            val moduleList: List<Module> = listOf(
                apiModule,
                viewModelModule,
                repositoryModule,
                networkModule)

            modules(moduleList)
        }

    }
}