package com.app.amber.location.model.request

import com.google.gson.annotations.SerializedName

data class WeatherRequest(
    @SerializedName("lat")
    var lat: Double? = null,

    @SerializedName("lon")
    var lon: Double? = null,

    @SerializedName("appid")
    var appId: String? = null

)