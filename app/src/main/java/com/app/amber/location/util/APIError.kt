package com.app.amber.location.util

data class APIError(val message: String) {
    constructor() : this("")
}
