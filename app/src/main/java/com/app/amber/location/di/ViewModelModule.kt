package com.app.amber.location.di

import com.app.amber.location.viewmodel.LocationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { LocationViewModel(repository = get(),get()) }


}