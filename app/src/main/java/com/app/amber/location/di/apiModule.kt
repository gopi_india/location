package com.app.amber.location.di

import com.app.amber.location.api.WeatherApi
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {

    fun provideWeatherApi(retrofit: Retrofit): WeatherApi {
        return retrofit.create(WeatherApi::class.java)
    }
    single { provideWeatherApi(get()) }

}