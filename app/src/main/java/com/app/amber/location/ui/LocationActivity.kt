package com.app.amber.location.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.app.amber.location.R
import com.app.amber.location.databinding.ActivityLocationBinding
import com.app.amber.location.util.Constants.GPS_REQUEST
import com.app.amber.location.util.Constants.LOCATION_REQUEST
import com.app.amber.location.util.GpsUtils
import com.app.amber.location.viewmodel.LocationViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.android.viewmodel.ext.android.viewModel


class LocationActivity : AppCompatActivity() {

    private val locationViewModel by viewModel<LocationViewModel>()
    private var isGPSEnabled = false
    lateinit var mLocationBinding: ActivityLocationBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLocationBinding = ActivityLocationBinding.inflate(layoutInflater)
        setContentView(mLocationBinding.getRoot())


        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                this@LocationActivity.isGPSEnabled = isGPSEnable
            }
        })

        locationViewModel.weatherResponse.observe(this, Observer {
            try {
                val gson = GsonBuilder().setPrettyPrinting().create()
                mLocationBinding.response.setText(gson.toJson(it))
            } catch (ex: Exception) {
            }
        })
    }

    override fun onStart() {
        super.onStart()
        invokeLocationAction()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GPS_REQUEST) {
                isGPSEnabled = true
                invokeLocationAction()
            }
        }
    }

    private fun invokeLocationAction() {
        when {
            !isGPSEnabled -> mLocationBinding.latLong.text = getString(R.string.enable_gps)

            isPermissionsGranted() -> startLocationUpdate()

            shouldShowRequestPermissionRationale() -> mLocationBinding.latLong.text =
                getString(R.string.permission_request)
            else -> {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ),
                    LOCATION_REQUEST
                )
            }
        }
    }

    private fun startLocationUpdate() {
        locationViewModel.getLocationData().observe(this, Observer {
            mLocationBinding.latLong.text = getString(R.string.latLong, it.longitude, it.latitude)

            locationViewModel.weatherApiCall(it.latitude, it.longitude)

        })
    }


    private fun isPermissionsGranted() =
        ActivityCompat.checkSelfPermission(
            this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(
                    this, Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED

    private fun shouldShowRequestPermissionRationale() =
        ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) && ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_REQUEST -> {
                invokeLocationAction()
            }
        }
    }
}




