package com.app.amber.location.di

import android.content.Context
import com.app.amber.location.api.WeatherApi
import com.app.amber.location.repository.RemoteRepository
import com.app.amber.location.repository.RemoteRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {

    fun provideWeatherRepository(api: WeatherApi, context: Context): RemoteRepository {
        return RemoteRepositoryImpl(api, context)
    }

    single { provideWeatherRepository(get(), androidContext()) }

}