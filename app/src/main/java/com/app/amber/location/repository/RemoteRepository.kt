package com.app.amber.location.repository

import com.app.amber.location.model.request.WeatherRequest
import com.app.amber.location.model.response.WeatherResponse
import com.app.amber.location.util.AppResult


interface RemoteRepository {
    suspend fun weatherApiCall(param : WeatherRequest) : AppResult<WeatherResponse>
 }
