package com.app.amber.location.model.response


import com.google.gson.annotations.SerializedName

data class RainX(
    @SerializedName("1h")
    val h: Double
)