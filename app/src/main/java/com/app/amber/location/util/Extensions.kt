package com.app.amber.location.util

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.app.amber.location.R

fun Context.noNetworkConnectivityError(): AppResult.Error {
    return AppResult.Error(Exception(this.resources.getString(R.string.no_network_error)))
}

fun Context.checkSinglePermission(permission: String): Boolean {
    return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}
