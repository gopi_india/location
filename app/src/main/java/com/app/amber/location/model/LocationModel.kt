package com.app.amber.location.model

data class LocationModel(
    val longitude: Double,
    val latitude: Double
)