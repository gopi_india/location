package com.app.amber.location.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.amber.location.model.request.WeatherRequest
import com.app.amber.location.repository.RemoteRepository
import androidx.lifecycle.viewModelScope
import com.app.amber.location.model.response.WeatherResponse
import com.app.amber.location.util.AppResult
import kotlinx.coroutines.launch

class LocationViewModel(var repository: RemoteRepository, context: Context) : ViewModel() {

    private val locationData = LocationLiveData(context)

    fun getLocationData() = locationData
    val weatherResponse = MutableLiveData<WeatherResponse>()


    fun weatherApiCall(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            val weatherReq = WeatherRequest(latitude, longitude, "10a24bab56575f06504187daa1f6c0ba")
            val result = repository.weatherApiCall(weatherReq)
            when (result) {

                is AppResult.Success -> {
                     weatherResponse.value = result.successData
                }

                is AppResult.Error -> {
                    Log.d("response", result.exception.localizedMessage)
                }
            }

        }
    }

}
