package com.app.amber.location.api

import com.app.amber.location.model.request.WeatherRequest
import com.app.amber.location.model.response.WeatherResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("/data/2.5/onecall?")
    suspend fun weatherCall(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") key: String,
    ): Response<WeatherResponse>

}